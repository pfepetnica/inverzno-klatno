// Moram imati klasu koja prestavlja paket upravljanja pod nazivom Actuation
class Actuation
{
  float silaoslonca = 0;
}

float givenX = 0;

/*// Moram imati klasu koja prestavlja merni rezultat pod nazivom Measurement
 void keyPressed()
 {
 if (key=='a')
 {
 givenX = -5;
 }
 else if (key=='d')
 {
 givenX = 5;
 }
 else {givenX=0;}
 }*/

class Measurement
{
  float onoStoMerite = 0;
}

// Moja simulacija mora da nasledi klasu Simulation i da implementira
// funkcije Update, Measure, Control, Visualize
class MojaSimulacija extends Simulation
{
  float alfa1=0;
  float alfa2=0;
  float alfapre=0;
  float zadatoX = 0;
  float nekaVrednost = 0;
  float dt;
  float v=0;
  float MI=1/3;
  float ubrzanje=0;
  float alfa=0.3;
  float x1=350, x2, y1=180, y2;
  float masa=1;
  float l=1;
  float g=9.81;
  float w=0;
  float t=0;
  float k=0;
  float Force, tg;
  float a;
  float kp=15,ki,kd=5;
  float e;
  MojaSimulacija ()
  {
    // Podesim frekvenciju update-jta simulacije da bude 10 puta veca od
    // frame rate-jta (30 fps)
    SetUpdateMul(10);

    // Podesim periodu kontrolera tako da bude 5 puta manja od perioda simulacije
    SetControlMul(5);

    dt = GetUpdatePeriod();
  }

  // Napravim korak simulacije. Npr. uradim numericku integraciju diferencijalne jednacine.
  // Dato mi je i upravljanje koje je izgenerisao kontroler.
  void Update(Actuation input)
  {
    //t=t+dt;
    //kp=10;
    MI=(masa*l*l)/3;
    Force=input.silaoslonca;
    ubrzanje=((masa*g*sin(alfa)+cos(alfa)*Force)*(l/2))/MI;
    w=w+ubrzanje*dt;
    //println(ubrzanje);
    alfa1=alfa;
    alfa =alfa+w*dt;
    println(alfa-alfa1);
    a=(Force/masa);
    v=v+a*dt;
    x1=x1+v*dt;
    //println(v);
    //   alfa1=alfa;
  }
  // Napravim merenja (merenja se uvek desava pre izvrsavanja kontrolera)
  Measurement Measure()
  {
    Measurement m = new Measurement();
    m.onoStoMerite =-5;
    return m;
  }

  // Izracunam novo upravljanje na osnovu merenja
  Actuation Control(Measurement m)
  {
    float F;
    float dalfa;
    e=-alfa;
    Actuation o = new Actuation();
   dalfa=alfa2-alfa;
      F=kp*e;
    F=F+kd*(alfa-alfa1);
    o.silaoslonca=F;
    return o;
  }

  // Vizualizujem rezultat simulacije (frame rate je 30 fps)
  void Visualize()
  {
    x2=(x1+(l)*100*sin(alfa));
    y2=(y1-(l)*100*cos(alfa));
    //println(alfa);

    background(204);

    line((int)(x1), (int)(y1), x2, y2);

    /*
    stroke(255, 0, 0);
     line(0, 100, width, 100); 
     stroke(255);
     line(0, nekaVrednost, width, nekaVrednost);*/
  }
}

// Moram implementirati funkciju koja inicijalizuje simulaciju
// i u njoj postaviti globalnu promenljivu sim na instancu moje simulacije
void InitSimulation()
{
  surface.setSize(640, 360);
  stroke(255);

  sim = new MojaSimulacija();
}